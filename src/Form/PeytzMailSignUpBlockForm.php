<?php
/**
 * @file
 * Contains \Drupal\peytz_mail\Form\PeytzMailSignUpBlockForm.
 */

namespace Drupal\peytz_mail\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\peytz_mail\Form;

/**
 * {@inheritdoc}
 */
class PeytzMailSignUpBlockForm extends PeytzMailSignUpFormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'peytz_mail_sign_up_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, array $configuration = array()) {
    return parent::buildForm($form, $form_state, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }

}
